Instructivo de instalación

1. Descargar el proyecto desde https://gitlab.com/opera-digital/registrator.git
2. Ejecutar el comando `yarn` para descargar las dependencias.
3. Ejecutar el comando `cd backend` para acceder a la carpeta con los archivos del backend. Desde esta carpeta ejecutar el comando `yarn dev` para iniciar el servidor. Por defecto está configurado para ejecutarse en el puerto 8080.
4. Ejecutar el comando `cd frontend` para acceder a la carpeta con los archivos del frontend. Ejecutar el comando `yarn start` para ver el proyecto en el navegador.

Endpoints

* `POST` http://localhost:8080/workers/register 
* `GET` http://localhost:8080/workers/list 
* `POST` http://localhost:8080/workers/:id

Tecnologías

La aplicación fue construida utilizando el stack MERN. 

MONGODB para la base de datos.
EXPRESS como framework para el servidor.
REACT es el encargado de la interfaz gráfica.
NODE es el backend.

Adicionalmente se utilizaron las siguientes librerías en el backend:

Cors.
Mongoose.
Dotenv.

Las librerías adicionales del frontend fueron:

Axios.
React Router DOM.
SASS.

Los estilos de la aplicación fueron construidos usando SASS. Para el la disposición de los elementos y el layout se utilizó Flexbox y CSS Grid.

