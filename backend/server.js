const express = require('express')
const cors = require('cors')
const workerRouter = require('./src/views/worker')
const { connect } = require('./src/db')
require('dotenv').config()

const app = express()
connect()

app.use(cors())
app.use(express.json())
app.use('/workers', workerRouter)

app.listen(process.env.PORT, () => {
  console.log('Server is Ready')
})
