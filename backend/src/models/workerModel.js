const { model, Schema } = require('mongoose')
const pattern = new RegExp('^[a-zA-Zs]*$')
const patternWithSpace = new RegExp('^[a-zA-Zs ]*$')
const alphaNumeric = new RegExp('^[a-zA-Z0-9_]*$')

const workerSchema = new Schema(
  {
    firstLastName: {
      match: [pattern, 'Solo se permiten letras'],
      required: [true, 'Please enter your sur name'],
      type: String,
      maxlength: [20, 'el apellido es demasiado largo'],
    },
    secondLastName: {
      match: [pattern, 'Solo se permiten letras'],
      required: [true, 'Please enter your second last name'],
      type: String,
      maxlength: [20, 'el apellido es demasiado largo'],
    },
    firstName: {
      match: [pattern, 'Solo se permiten letras'],
      required: [true, 'Please enter your first name'],
      type: String,
      maxlength: [20, 'el nombre es demasiado largo'],
    },
    middleName: {
      match: [patternWithSpace, 'Solo se permiten letras'],
      type: String,
      maxlength: [50, 'el nombre es demasiado largo'],
    },
    country: {
      required: [true, 'Please select a country'],
      type: String,
      enum: ['Colombia', 'United States'],
      default: 'Colombia',
    },
    idType: {
      required: [true, 'Please select your document type'],
      type: String,
      enum: [
        'Cédula de Ciudadanía',
        'Cédula de Extranjería',
        'Pasaporte',
        'Permiso Especial',
      ],
      default: 'Cédula de Ciudadanía',
    },
    idNumber: {
      match: [alphaNumeric, 'Solo se permiten números y letras'],
      maxlength: [20, 'el nombre es demasiado largo'],
      required: [true, 'Please enter your ID number'],
      unique: true,
      type: String,
    },
    email: {
      type: String,
    },
    startDate: {
      required: [true, 'Por favor, indica la fecha de ingreso del trabajador'],
      type: Date,
    },
    area: {
      required: [
        true,
        'Por favor, indica para cuál área está asignado este trabajador',
      ],
      type: String,
      enum: [
        'Administración',
        'Financiera',
        'Compras',
        'Infraestructura',
        'Operación',
        'Talento Humano',
        'Servicios Varios',
      ],
      default: 'Administración',
    },
    state: {
      type: Boolean,
      default: true,
    },
  },
  { timestamps: true }
)

const Worker = model('Worker', workerSchema)

module.exports = Worker
