const router = require('express').Router()
const workerController = require('../controllers/workerController')

router.route('/register').post(workerController.register)
router.route('/list').get(workerController.list)
router.route('/:id').post(workerController.edit)

module.exports = router
