const mongoose = require('mongoose')

const connect = () => {
  const opts = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  }
  mongoose.connect(process.env.URI, opts)

  mongoose.connection.once('open', () => {
    console.log('DATABASE Ready')
  })

  mongoose.connection.on('error', () => {
    console.log('something wrong with the database connection')
  })

  return mongoose.connection
}

module.exports = { connect }
