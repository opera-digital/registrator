const Worker = require('../models/workerModel')

module.exports = {
  async register(req, res) {
    try {
      const { firstLastName, firstName } = req.body

      const worker = await Worker.create({ ...req.body })

      let region = ''

      if (worker.country === 'Colombia') {
        region = '.co'
      } else {
        region = '.us'
      }

      worker.email = `${
        firstName + firstLastName
      }@cidenet.com${region}`.toLowerCase()

      worker.save({ validateBeforeSave: false })

      res.status(201).json({ message: 'New Worker Created', worker })
    } catch (err) {
      res.status(400).json({ message: 'something wrong', err: err.message })
    }
  },
  async list(req, res) {
    const workers = await Worker.find()

    res.status(200).json({ workers })
  },

  async edit(req, res) {
    try {
      const data = req.body
      const { id } = req.params
      await Worker.findByIdAndUpdate(
        id,
        data,
        { new: true },
        function (err, docs) {
          if (err) {
            console.log(err)
          } else {
            console.log('updated', docs)
          }
        }
      )
      res.status(200).json({ message: 'Worker updated' })
    } catch (err) {
      res
        .status(400)
        .json({ message: 'something wrong with the update', err: err.message })
    }
  },
}
