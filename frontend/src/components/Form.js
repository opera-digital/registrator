import React, { useContext } from 'react'
import axios from 'axios'
import sendIcon from '../assets/send.svg'
import { WorkerContext } from './workerContext'
import { useHistory } from 'react-router'

const Form = () => {
  const { worker, setWorker } = useContext(WorkerContext)

  const {
    firstLastName,
    secondLastName,
    firstName,
    middleName,
    country,
    idType,
    idNumber,
    startDate,
    area,
    // registration,
  } = worker

  const handleChange = (e) => {
    const { name, value } = e.target
    setWorker({ ...worker, [name]: value })
  }
  const history = useHistory()

  const handleSubmit = async (e) => {
    e.preventDefault()

    const data = worker

    await axios({
      method: 'POST',
      baseURL: 'http://localhost:8080',
      url: '/workers/register',
      data: data,
    })
    history.push('/list')
    console.log(worker)
  }
  return (
    <form className="form" onSubmit={handleSubmit}>
      <div className="left">
        <div className="field">
          <label htmlFor="firstLastName">Last Name</label>
          <input
            type="text"
            name="firstLastName"
            value={firstLastName}
            onChange={handleChange}
            autoComplete="off"
          />
        </div>
        <div className="field">
          <label htmlFor="secondLastName">Second Last Name</label>
          <input
            type="text"
            name="secondLastName"
            value={secondLastName}
            onChange={handleChange}
            autoComplete="off"
          />
        </div>
        <div className="field">
          <label htmlFor="firstName">First Name</label>
          <input
            type="text"
            name="firstName"
            value={firstName}
            onChange={handleChange}
            autoComplete="off"
          />
        </div>
        <div className="field">
          <label htmlFor="last_name">Middle Name</label>
          <input
            type="text"
            name="middleName"
            value={middleName}
            onChange={handleChange}
            autoComplete="off"
          />
        </div>
        <div className="field">
          <label htmlFor="country">Country</label>
          <select name="country" onChange={handleChange} value={country}>
            <option value="Colombia">Colombia</option>
            <option value="United States">United States</option>
          </select>
        </div>
      </div>
      <div className="right">
        <div className="field">
          <label htmlFor="idType">Identification Type</label>
          <select name="idType" onChange={handleChange} value={idType}>
            <option value="Cédula de Ciudadanía">Cédula de Ciudadanía</option>
            <option value="Cédula de Extranjería">Cédula de Extranjería</option>
            <option value="Pasaporte">Pasaporte</option>
            <option value="Permiso Especial">Permiso Especial</option>
          </select>
        </div>
        <div className="field">
          <label htmlFor="idNumber">ID Number</label>
          <input
            type="text"
            name="idNumber"
            value={idNumber}
            onChange={handleChange}
            autoComplete="off"
          />
        </div>
        <div className="field">
          <label htmlFor="startDate">Start Date</label>
          <input
            type="date"
            name="startDate"
            value={startDate}
            onChange={handleChange}
          />
        </div>
        <div className="field">
          <label htmlFor="area">Area</label>
          <select name="area" onChange={handleChange} value={area}>
            <option value="Administración">Administración</option>
            <option value="Financiera">Financiera</option>
            <option value="Compras">Compras</option>
            <option value="Infraestructura">Infraestructura</option>
            <option value="Operación">Operación</option>
            <option value="Talento Humano">Talento Humano</option>
            <option value="Servicios Varios">Servicios Varios</option>
          </select>
        </div>
        {/* <div className="field">
              <label htmlFor="registration">Registrated at</label>
              <input
                type="date"
                name="registration"
                value={registration}
                onChange={handleChange}
                autoComplete="off"
              />
            </div> */}
        <div className="field">
          <button className="button" type="submit">
            <h3>Submit</h3>
            <img src={sendIcon} alt="send icon" />
          </button>
        </div>
      </div>
    </form>
  )
}

export default Form
