import React from 'react'
import { Link } from 'react-router-dom'
import addIcon from '../assets/add.svg'
import listIcon from '../assets/list.svg'
import logo from '../assets/logo.svg'

const Menu = () => {
  return (
    <div className="menu">
      <div className="logo">
        <img src={logo} alt="logo" />
      </div>

      <div className="menu-buttons">
        <Link style={{ textDecoration: 'none' }} to="/register">
          <button className="button">
            <h3>ADD NEW</h3>

            <img src={addIcon} alt="Add worker icon" />
          </button>
        </Link>
        <Link style={{ textDecoration: 'none' }} to="/list">
          <button className="button">
            <h3>WORKERS</h3>

            <img src={listIcon} alt="List workers icon" />
          </button>
        </Link>
      </div>
    </div>
  )
}

export default Menu
