import React from 'react'
import Form from './Form'

const Edit = () => {
  return (
    <div className="content">
      <div className="register">
        <h2 className="title">Worker Edition</h2>
        <Form />
      </div>
    </div>
  )
}

export default Edit
