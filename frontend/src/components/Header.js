import React, { useState } from 'react'
import searchIcon from '../assets/search.svg'

const Header = () => {
  return (
    <form className="header">
      <div className="search">
        <input
          type="text"
          placeholder="Search a worker"
          className="search-input"
        />
        <button className="search-icon" type="submit">
          <img src={searchIcon} alt="Search Icon" />
        </button>
      </div>
    </form>
  )
}

export default Header
