import React from 'react'

import Form from './Form'

const Register = () => {
  return (
    <div className="content">
      <div className="register">
        <h2 className="title">Worker Registration</h2>
        <Form />
      </div>
    </div>
  )
}

export default Register
