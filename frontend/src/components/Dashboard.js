import React from 'react'
import Header from './Header'
import Menu from './Menu'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Register from './Register'
import List from './List'
import Edit from './Edit'

const Dashboard = () => {
  return (
    <div className="dashboard">
      <Router>
        <Header />
        <Menu />
        <div className="content">
          <Switch>
            <Route exact path="/register" component={Register} />
            <Route exact path="/list" component={List} />
            <Route exact path="/:id" component={Edit} />
          </Switch>
        </div>
      </Router>
    </div>
  )
}

export default Dashboard
