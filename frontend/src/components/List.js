import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router'
import editIcon from '../assets/edit.svg'

const List = () => {
  const [workers, setWorkers] = useState([])

  const requestWorkers = () => {
    axios({
      method: 'GET',
      baseURL: 'http://localhost:8080',
      url: '/workers/list',
    })
      .then(({ data }) => {
        setWorkers(data.workers)
      })
      .catch((err) => {
        console.log(err)
      })
  }

  useEffect(() => {
    requestWorkers()
  }, [])

  const history = useHistory()

  console.log(workers)

  return (
    <div className="content">
      <h2 className="title">Cidenet Workers</h2>
      <table className="list">
        <thead>
          <tr>
            <th scope="col">Last Name</th>
            <th scope="col">Snd. Last Name</th>
            <th scope="col">First Name</th>
            <th scope="col">Middle Name</th>
            <th scope="col">Email</th>
            <th scope="col">ID Type</th>
            <th scope="col">ID Number</th>
            <th scope="col">Area</th>
            <th scope="col">Registered at</th>
          </tr>
        </thead>
        <tbody>
          {workers.map((worker) => {
            return (
              <tr className="worker-registry" key={worker._id}>
                <td>{worker.firstLastName}</td>
                <td>{worker.secondLastName}</td>
                <td>{worker.firstName}</td>
                <td>{worker.middleName}</td>
                <td>{worker.email}</td>
                <td>{worker.idType}</td>
                <td>{worker.idNumber}</td>
                <td>{worker.area}</td>
                <td>{worker.startDate}</td>
                <td>
                  <img
                    src={editIcon}
                    alt="edit icon"
                    style={{ width: '2.5rem' }}
                    onClick={() => history.push(`/${worker._id}`)}
                  />
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  )
}

export default List
