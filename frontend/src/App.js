import React, { useState } from 'react'
import Dashboard from './components/Dashboard'
import { WorkerContext } from './components/workerContext'

const App = () => {
  const [worker, setWorker] = useState({
    firstLastName: '',
    secondLastName: '',
    firstName: '',
    middleName: '',
    country: 'Colombia',
    idType: 'Cédula de Ciudadanía',
    idNumber: '',
    startDate: '',
    area: 'Administración',
    // registration: '',
  })

  return (
    <WorkerContext.Provider value={{ worker, setWorker }}>
      <Dashboard />
    </WorkerContext.Provider>
  )
}

export default App
